#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

. /etc/lsb-release

export DEBIAN_FRONTEND=noninteractive
export SCCACHE_DIR="${CI_PROJECT_DIR}/sccache"
export RUSTC_WRAPPER=/usr/bin/sccache
mkdir -p "${SCCACHE_DIR}"
tag="${CI_COMMIT_TAG:-0.0+0}"
version="${tag%%+*}"
tmpdir="$(mktemp -d)"
nfpm="${NFPM:-2.35.3}"
architecture="$(dpkg --print-architecture)"
case "${architecture}" in
    "amd64")
        goarch="x86_64"
        ;;
esac
apt-get update
apt-get \
    -y \
    --no-install-recommends \
    install \
    apt-file \
    ca-certificates \
    cargo \
    cmake \
    curl \
    desktop-file-utils \
    gcc \
    gettext \
    git \
    libadwaita-1-dev \
    libdbus-1-dev \
    libgstreamer-plugins-bad1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer1.0-dev \
    libgtk-4-bin \
    libgtk-4-dev \
    meson \
    reuse \
    rustc \
    sccache \
    valac
apt-file update
if [ ! -d amberol ]; then
    git clone -b "${version}" https://gitlab.gnome.org/World/amberol
fi
cd amberol
meson setup build --buildtype=release --prefix /usr
meson install -C build --destdir "${tmpdir}"
mkdir -p "${CI_PROJECT_DIR}/${CODENAME}"
if ! command -v nfpm >/dev/null 2>&1; then
    curl -L \
        "https://github.com/goreleaser/nfpm/releases/download/v${nfpm}/nfpm_${nfpm}_Linux_${goarch}.tar.gz" |
        tar -xzf - -C /usr/bin nfpm
fi
ldd "${tmpdir}"/usr/bin/amberol | grep '=>' | awk '{print $3}' >"/tmp/deps.libs"
mapfile -t deps < <(apt-file --architecture "${architecture}" find -f /tmp/deps.libs | awk -F ':' '{print $1}' | sort -u | grep -v '.*-dev')
cat >"${tmpdir}/nfpm.yaml" <<EOF
name: amberol
arch: ${architecture}
version: ${tag//[a-zA-Z]/}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: A small and simple sound and music player that is well integrated with GNOME.
homepage: https://gitlab.gnome.org/World/amberol
contents:
  - src: ${tmpdir}/usr/
    dst: /usr
    type: tree
scripts:
  preinstall: ${CI_PROJECT_DIR}/.packaging/preinstall.sh
recommends:
  - morph027-keyring
depends:
  - libgtk-4-bin
EOF
for dep in "${deps[@]}"; do
    echo "  - ${dep}" >>"${tmpdir}/nfpm.yaml"
done
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
mkdir -p "${CI_PROJECT_DIR}/${DISTRIB_CODENAME}"
cp ./*.deb "${CI_PROJECT_DIR}/${DISTRIB_CODENAME}"
