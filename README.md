# [Amberol](https://gitlab.gnome.org/World/amberol) Packages

Native packages without flatpak, just build using meson.

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-amberol.asc https://packaging.gitlab.io/amberol/gpg.key
```

## Add repo to apt

```bash
. /etc/lsb-release
echo "deb [arch=amd64] https://packaging.gitlab.io/amberol/${DISTRIB_CODENAME} ${DISTRIB_CODENAME} main" | sudo tee /etc/apt/sources.list.d/morph027-amberol.list
```

## Install packages

```bash
sudo apt-get update
sudo apt-get install amberol morph027-keyring
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
echo 'Unattended-Upgrade::Origins-Pattern {"origin=morph027,codename=${distro_codename},label=amberol";};' | sudo tee /etc/apt/apt.conf.d/50amberol
```
